package furqan.albarado.uts_3c_katalog

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Barang(var idBarang : String, var namaBarang : String, var hargaBarang : String,
             var deskBarang : String, var stokBarang : Int, var namaFile : String,var fileProduk : String) : Parcelable {
    constructor() : this("","","","",0,"","")
}