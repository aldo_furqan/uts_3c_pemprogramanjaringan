package furqan.albarado.uts_3c_katalog

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import furqan.albarado.uts_3c_katalog.Barang
import furqan.albarado.uts_3c_katalog.HPFragment
import kotlinx.android.synthetic.main.row_barang.view.*

class BarangAdapter (items:List<Barang>, context: Context, val itemKlik: HPFragment): RecyclerView.Adapter<BarangAdapter.ViewHolder>(){

    private var list = items
    private var context = context

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BarangAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_barang,parent,false))
    }

    override fun onBindViewHolder(holder: BarangAdapter.ViewHolder, position: Int) {
        holder.namaBarang.text = list[position].namaBarang
        holder.hargaBarang.text = list[position].hargaBarang
        Picasso.get().load(list[position].fileProduk).into(holder.imageBarang)
        holder.root_item_barang.setOnClickListener {
           itemKlik.gotoDetail(list[position])
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(v: View): RecyclerView.ViewHolder(v){
        val namaBarang = v.textView_NamaBarang!!
        val hargaBarang = v.textView_HargaBarang!!
        val imageBarang = v.imageView_Barang!!
        val root_item_barang = v.root_item_produk!!
    }
}