package furqan.albarado.uts_3c_katalog

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_barang.*


class DetailBarangActivity : AppCompatActivity() {

    val barangCrud = Barang()
    val rowBarang = RowBarang()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_barang)

        initView()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_detail, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.menu_edit -> {
                val barang = intent.getParcelableExtra<Barang>("DetBarang")
                val intent = Intent(this, EditBarangActivity::class.java)
                intent.putExtra("DetBarang", barang)
                startActivity(intent)
            }
            R.id.menu_hapus -> {
                val builder: AlertDialog.Builder = AlertDialog.Builder(this)
                builder.setMessage("Apakah anda yakin mau menghapus?")
                    .setCancelable(false)
                    .setPositiveButton("Ya", DialogInterface.OnClickListener { dialog, id -> deleteData() })
                    .setNegativeButton("Tidak", DialogInterface.OnClickListener { dialog, id -> dialog.cancel() }) //Set your icon here
                    .setTitle("Hapus Data!")
                    .setIcon(R.drawable.ic_warning)
                val alert: AlertDialog = builder.create()
                alert.show()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initView() {
        supportActionBar?.title = "Detail Barang"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val barang = intent.getParcelableExtra<Barang>("DetBarang")

        Picasso.get().load(barang?.fileProduk).into(imageView_Barang_Detail)
        barangCrud.idBarang = barang?.idBarang.toString()
        editTextNama_Barang_Detail.setText(barang?.namaBarang)
        editText_Stok_Barang.setText(barang?.stokBarang.toString())
        editText_Harga.setText(barang?.hargaBarang)
        editText_Deskripsi_Detail_Barang.setText(barang?.deskBarang)
    }

    private fun deleteData(){
        val barang = intent.getParcelableExtra<Barang>("DetBarang")
        FirebaseStorage.getInstance().reference.child(barang?.namaFile.toString()).delete()
            .addOnSuccessListener {
                FirebaseFirestore.getInstance().collection("products").whereEqualTo(rowBarang.K_IDBARANG,barang?.idBarang).get()
                    .addOnSuccessListener { results ->
                        for (doc in results){
                            FirebaseFirestore.getInstance().collection("products").document(doc.id).delete()
                                .addOnSuccessListener {
                                    Komponen.ProgressLoading(false,"",this)
                                    onBackPressed()
                                }
                                .addOnFailureListener {
                                    Toast.makeText(this, "Data failed deleted",Toast.LENGTH_LONG).show()
                                }
                        }
                    }.addOnFailureListener { e ->
                        Toast.makeText(this, "Can't get data references ${e.message}",Toast.LENGTH_LONG).show()
                    }
            }
            .addOnFailureListener {
                Toast.makeText(this,it.message.toString(), Toast.LENGTH_LONG).show()
            }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}