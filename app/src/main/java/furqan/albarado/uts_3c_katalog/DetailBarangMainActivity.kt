package furqan.albarado.uts_3c_katalog

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.squareup.picasso.Picasso
import furqan.albarado.uts_3c_katalog.Barang
import kotlinx.android.synthetic.main.activity_detail_barang_main.*
import java.text.SimpleDateFormat
import java.util.*

class DetailBarangMainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_barang_main)

        initView()
    }

    private fun initView() {
        val barang = intent.getParcelableExtra<Barang>("DetBarangMain")

        supportActionBar?.title = barang!!.namaBarang
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        Picasso.get().load(barang?.fileProduk).into(imageView_Det_Main)
        textView_Barang_Main.setText(barang!!.namaBarang)
        textView_Harga_Main.setText(barang!!.hargaBarang)
        textView_Stok_Main.setText("Stok tersisa "+barang!!.stokBarang)
        textView_Deskripsi_Main.setText(barang!!.deskBarang)

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}