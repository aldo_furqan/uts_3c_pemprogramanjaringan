package furqan.albarado.uts_3c_katalog

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.activity_edit_barang.*
import java.text.SimpleDateFormat
import java.util.*

class EditBarangActivity: AppCompatActivity() {

    lateinit var uri : Uri
    var isInputFile : Boolean = false

    companion object {
        val K_IDBARANG = "idBarang"
        val K_NAMABARANG = "namaBarang"
        val K_HARGABARANG = "hargaBarang"
        val K_DESKBARANG = "deskBarang"
        val K_STOKBARANG = "stokBarang"
        val K_FILEPRODUK = "fileProduk"
        val K_NAMAFILE =  "namaFile"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_barang)

        initView()
    }

    private fun initView() {
        supportActionBar?.title = "Edit Barang Elektronik"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val barang = intent.getParcelableExtra<Barang>("DetBarang")
        editText_Nama_Edit_Barang.setText(barang?.namaBarang)
        editText_Stok_Edit_Barang.setText(barang?.stokBarang.toString())
        editText_Harga_Edit_Barang.setText(barang?.hargaBarang)
        editText_selected_foto_edit_barang.setText(barang?.fileProduk)
        editText_deskripsi_edit_barang.setText(barang?.deskBarang)

        button_simpan_edit_barang.setOnClickListener {
            uploadDataBarang()
        }

        button_tambah_foto_edit_barang.setOnClickListener {
            val intent = Intent()
            intent.action = Intent.ACTION_GET_CONTENT
            intent.setType("image/*")
            startActivityForResult(intent, 100)
        }
    }

    private fun uploadDataBarang() {
        val barangId = intent.getParcelableExtra<Barang>("DetBarang")


        Komponen.ProgressLoading(true,"Harap Tunggu...",this)
        val fileName = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
        val fileRef = FirebaseStorage.getInstance().reference.child(fileName+".jpg")
        if(isInputFile){
            // Delete File Sebelumnya
            FirebaseStorage.getInstance().reference.child(barangId?.namaFile.toString()).delete()
                .addOnSuccessListener {
                    fileRef.putFile(uri)
                        .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                            return@Continuation fileRef.downloadUrl
                        })
                        .addOnCompleteListener { task ->
                            val hm = HashMap<String, Any>()
                            hm.set(K_IDBARANG, barangId?.idBarang.toString())
                            hm.set(K_NAMABARANG, editText_Nama_Edit_Barang.text.toString())
                            hm.set(K_HARGABARANG, editText_Harga_Edit_Barang.text.toString())
                            hm.set(K_DESKBARANG, editText_deskripsi_edit_barang.text.toString())
                            hm.set(K_STOKBARANG, editText_Stok_Edit_Barang.text.toString())
                            hm.set(K_NAMAFILE,fileName+".jpg")
                            hm.set(K_FILEPRODUK, task.result.toString())
                            updateDataBarang(barangId?.idBarang.toString(),hm)
                        }
                }
                .addOnFailureListener {
                    Toast.makeText(this,it.message.toString(), Toast.LENGTH_LONG).show()
                }
        }else{
            val hm = HashMap<String, Any>()
            hm.set(K_IDBARANG, barangId?.idBarang.toString())
            hm.set(K_NAMABARANG, editText_Nama_Edit_Barang.text.toString())
            hm.set(K_HARGABARANG, editText_Harga_Edit_Barang.text.toString())
            hm.set(K_DESKBARANG, editText_deskripsi_edit_barang.text.toString())
            hm.set(K_STOKBARANG, editText_Stok_Edit_Barang.text.toString())
            updateDataBarang(barangId?.idBarang.toString(),hm)
        }
    }

    private fun updateDataBarang(barangId : String, hm : HashMap<String, Any>){
        FirebaseFirestore.getInstance().collection("products").document(barangId).update(hm)
            .addOnSuccessListener {
                Komponen.ProgressLoading(false,"",this)
                onBackPressed()
            }
            .addOnFailureListener {
                Toast.makeText(this, "Data failed to added ${it.message}", Toast.LENGTH_LONG).show()
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if((resultCode == Activity.RESULT_OK) && (requestCode == 100)){
            if(data != null){
                isInputFile = true
                Toast.makeText(this,isInputFile.toString(), Toast.LENGTH_LONG).show()
                uri = data.data!!
                editText_selected_foto_edit_barang.setText(uri.toString())
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}