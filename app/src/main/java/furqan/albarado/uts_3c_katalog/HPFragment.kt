package furqan.albarado.uts_3c_katalog

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_h_p.view.*

class HPFragment : Fragment() {

    lateinit var thisParent : MainActivity
    lateinit var v : View
    lateinit var db : CollectionReference
    var listProduct : MutableList<Barang> = ArrayList()
    val rowBarang = RowBarang()

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.fragment_h_p, container, false)
        initView()
        return v
    }

    private fun initView() {
        v.fab_add_barang.setOnClickListener {
            val intent = Intent(thisParent, TambahBarangActivity::class.java)
            startActivity(intent)
        }
    }

    private fun showDataBarang(){
        db = FirebaseFirestore.getInstance().collection("products")
        db.addSnapshotListener { value, error ->
            if(error != null){
                Log.e("firestore : ",error.message.toString())
            }
            db.get().addOnSuccessListener { results ->
                listProduct.clear()
                for (doc in results){
                    val barang = Barang()
                    barang.namaBarang = doc.get(rowBarang.K_NAMABARANG).toString()
                    barang.idBarang = doc.get(rowBarang.K_IDBARANG).toString()
                    barang.stokBarang = doc.get(rowBarang.K_STOKBARANG).toString().toInt()
                    barang.deskBarang = doc.get(rowBarang.K_DESKBARANG).toString()
                    barang.hargaBarang = doc.get(rowBarang.K_HARGABARANG).toString()
                    barang.namaFile = doc.get(rowBarang.K_NAMAFILE).toString()
                    barang.fileProduk = doc.get(rowBarang.K_FILEPRODUK).toString()
                    listProduct.add(barang)
                }
                v.recylerview_data_barang.adapter = BarangAdapter(listProduct,thisParent,this)
            }
        }
    }

    fun gotoDetail(list : Barang){
        val intent = Intent(thisParent,DetailBarangActivity::class.java)
        intent.putExtra("DetBarang",list)
        startActivity(intent)
    }

    override fun onStart() {
        super.onStart()
        showDataBarang()
    }


}