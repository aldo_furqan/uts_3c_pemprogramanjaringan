package furqan.albarado.uts_3c_katalog

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import furqan.albarado.uts_3c_katalog.Barang
import furqan.albarado.uts_3c_katalog.DetailBarangMainActivity
import kotlinx.android.synthetic.main.fragment_home.view.*


class HomeFragment : Fragment() {

    lateinit var thisParent : MainActivity
    lateinit var v : View
    lateinit var db : CollectionReference
    var listBarang : MutableList<Barang> = ArrayList()
    val rowBarang = RowBarang()

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.fragment_home, container, false)
        v.recyclerView_DataHome.setLayoutManager(GridLayoutManager(thisParent,2))
        return v
    }

    private fun showDataBarang(){
        db = FirebaseFirestore.getInstance().collection("products")
        db.addSnapshotListener { value, error ->
            if(error != null){
                Log.e("firestore : ", error.message.toString())
            }
            db.get().addOnSuccessListener { results ->
                listBarang.clear()
                for (doc in results){
                    val barang = Barang()
                    barang.namaBarang = doc.get(rowBarang.K_NAMABARANG).toString()
                    barang.idBarang = doc.get(rowBarang.K_IDBARANG).toString()
                    barang.stokBarang = doc.get(rowBarang.K_STOKBARANG).toString().toInt()
                    barang.deskBarang = doc.get(rowBarang.K_DESKBARANG).toString()
                    barang.hargaBarang = doc.get(rowBarang.K_HARGABARANG).toString()
                    barang.namaFile = doc.get(rowBarang.K_NAMAFILE).toString()
                    barang.fileProduk = doc.get(rowBarang.K_FILEPRODUK).toString()
                    listBarang.add(barang)
                }
                v.recyclerView_DataHome.adapter = ListBarangHomeAdapter(listBarang, thisParent,this)
            }
        }
    }

    fun gotoDetailMain(list : Barang){
        val intent = Intent(thisParent, DetailBarangMainActivity::class.java)
        intent.putExtra("DetBarangMain",list)
        startActivity(intent)
    }

    override fun onStart() {
        super.onStart()
        showDataBarang()
    }

}