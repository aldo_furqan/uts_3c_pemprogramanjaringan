package furqan.albarado.uts_3c_katalog

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.row_list_barang_home.view.*
import kotlinx.android.synthetic.main.row_list_barang_home.view.*

class ListBarangHomeCustomerAdapter  (items:List<Barang>, context: Context, val itemKlik: MainActivityCustomer): RecyclerView.Adapter<ListBarangHomeCustomerAdapter.ViewHolder>(){

    private var list = items
    private var context = context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListBarangHomeCustomerAdapter.ViewHolder {
        return ListBarangHomeCustomerAdapter.ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_list_barang_home, parent, false))
    }

    override fun onBindViewHolder(holder: ListBarangHomeCustomerAdapter.ViewHolder, position: Int) {
        holder.namaBarang.text = list[position].namaBarang
        holder.hargaBarang.text = list[position].hargaBarang
        Picasso.get().load(list[position].fileProduk).into(holder.imageBarang)
        holder.root_barang_home.setOnClickListener {
            itemKlik.gotoDetailMain(list[position])
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class ViewHolder(v: View): RecyclerView.ViewHolder(v){
        val namaBarang = v.textView_Nama_Barang_Home!!
        val hargaBarang = v.textView_Harga_Barang_Home!!
        val imageBarang = v.imageView_Barang_Home!!
        val root_barang_home = v.root_barang_home!!
    }
}