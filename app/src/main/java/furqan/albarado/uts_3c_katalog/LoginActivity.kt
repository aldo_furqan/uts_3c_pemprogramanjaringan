package furqan.albarado.uts_3c_katalog

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import furqan.albarado.uts_3c_katalog.Komponen.Companion.ProgressLoading
import furqan.albarado.uts_3c_katalog.R
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btnLogin.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.btnLogin -> {
                var email = edUserName.text.toString()
                var password = edPassword.text.toString()
                if (email.isEmpty() || password.isEmpty()) {
                    Toast.makeText(this, "Username / password can't be empty", Toast.LENGTH_LONG)
                        .show()
                } else {
                    val progressDialog = ProgressDialog(this)
                    progressDialog.isIndeterminate = true
                    progressDialog.setMessage("Authentication...")
                    progressDialog.show()

                    FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener() {
                            progressDialog.hide()
                            if (!it.isSuccessful) return@addOnCompleteListener
                            Toast.makeText(
                                this,
                                "Successfully Login", Toast.LENGTH_SHORT
                            ).show()
                            val intent = Intent(this, MainActivity::class.java)
                            startActivity(intent)
                        }
                        .addOnFailureListener {
                            progressDialog.hide()
                            Toast.makeText(
                                this,
                                "Username/password incorrect", Toast.LENGTH_SHORT
                            ).show()
                        }
                }
            }
        }
    }

}