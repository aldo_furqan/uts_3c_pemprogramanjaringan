package furqan.albarado.uts_3c_katalog

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener{

    lateinit var ft : FragmentTransaction
    lateinit var hpFragment: HPFragment
    lateinit var homeFragment: HomeFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        hpFragment = HPFragment()
        homeFragment = HomeFragment()
        initView()
        cekUserIsLogin()
    }

    private fun initView() {
        bottomNavigationView.setOnNavigationItemSelectedListener(this)
    }

    private fun cekUserIsLogin(){
        val uid = FirebaseAuth.getInstance().uid
        if(uid == null){
            val intent = Intent(this, MainActivityCustomer::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.menu_barang -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayoutCustomer, hpFragment).commit()
            }
            R.id.menu_home -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayoutCustomer, homeFragment).commit()
            }
        }
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        menuInflater.inflate(R.menu.menu_option,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemRegister ->{
                var intent = Intent(this, RegisterActivity::class.java)
                startActivity(intent)
            }
            R.id.itemLogout ->{
                var fbAuth = FirebaseAuth.getInstance()
                fbAuth.signOut()
                var intent = Intent(this, MainActivityCustomer::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}