package furqan.albarado.uts_3c_katalog

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main_customer.*

class MainActivityCustomer : AppCompatActivity() {

    lateinit var db : CollectionReference
    var listBarang : MutableList<Barang> = ArrayList()
    val rowBarang = RowBarang()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_customer)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        menuInflater.inflate(R.menu.menu_option_customer,menu)
        return super.onCreateOptionsMenu(menu)
    }

    private fun showDataBarang(){
        db = FirebaseFirestore.getInstance().collection("products")
        db.addSnapshotListener { value, error ->
            if(error != null){
                Log.e("firestore : ", error.message.toString())
            }
            db.get().addOnSuccessListener { results ->
                listBarang.clear()
                for (doc in results){
                    val barang = Barang()
                    barang.namaBarang = doc.get(rowBarang.K_NAMABARANG).toString()
                    barang.idBarang = doc.get(rowBarang.K_IDBARANG).toString()
                    barang.stokBarang = doc.get(rowBarang.K_STOKBARANG).toString().toInt()
                    barang.deskBarang = doc.get(rowBarang.K_DESKBARANG).toString()
                    barang.hargaBarang = doc.get(rowBarang.K_HARGABARANG).toString()
                    barang.namaFile = doc.get(rowBarang.K_NAMAFILE).toString()
                    barang.fileProduk = doc.get(rowBarang.K_FILEPRODUK).toString()
                    listBarang.add(barang)
                }
                recyclerView_DataHome_Customer.adapter = ListBarangHomeCustomerAdapter(listBarang, this, this)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        showDataBarang()
    }

    fun gotoDetailMain(list : Barang){
        val intent = Intent(this, DetailBarangMainActivity::class.java)
        intent.putExtra("DetBarangMain",list)
        startActivity(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemLoginAdmin ->{
                var intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}