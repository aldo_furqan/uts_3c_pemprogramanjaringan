package furqan.albarado.uts_3c_katalog

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    val user = User()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        initView()
    }

    private fun initView() {
        button_register.setOnClickListener {
            register()
        }
    }

    private fun register(){
        val email = editTextEmailRegister.text.toString()
        val password = editTextPasswordRegister.text.toString()
        ProgressLoading(true,"Harap bersabar....")
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,password)
            .addOnCompleteListener {
                if(it.isSuccessful){
                    insertDataUser(0)
                    Toast.makeText(this, "Successfully Register", Toast.LENGTH_SHORT).show()
                }
            }
            .addOnFailureListener {
                ProgressLoading(false,"")
                Toast.makeText(this,"Failed to create user: ${it.message}", Toast.LENGTH_SHORT).show()
            }
    }

    private fun ProgressLoading(show : Boolean, message: String){
        val progressDialog = ProgressDialog(this)
        progressDialog.isIndeterminate = true
        progressDialog.setMessage(message)
        if(show){
            progressDialog.show()
        }else{
            progressDialog.hide()
        }
    }

    private fun insertDataUser(tipeuser : Int){
        val uid = FirebaseAuth.getInstance().uid ?: ""
        user.iduser = uid
        user.tipeuser = 0
        val email = editTextEmailRegister.text.toString()
        FirebaseFirestore.getInstance().collection("users").document(email).set(user)
            .addOnSuccessListener {
                ProgressLoading(false,"")
                val intent = Intent(this, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }
            .addOnFailureListener {
                Toast.makeText(this, "Data failed to added ${it.message}",Toast.LENGTH_LONG).show()
            }
    }

}