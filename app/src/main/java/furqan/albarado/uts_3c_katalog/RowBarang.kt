package furqan.albarado.uts_3c_katalog

class RowBarang {
    val K_IDBARANG = "idBarang"
    val K_NAMABARANG = "namaBarang"
    val K_HARGABARANG = "hargaBarang"
    val K_DESKBARANG = "deskBarang"
    val K_STOKBARANG = "stokBarang"
    val K_NAMAFILE = "namaFile"
    val K_FILEPRODUK = "fileProduk"
}