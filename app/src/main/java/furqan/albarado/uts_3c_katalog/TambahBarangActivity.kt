package furqan.albarado.uts_3c_katalog

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.activity_tambah_barang.*
import java.text.SimpleDateFormat
import java.util.*

class TambahBarangActivity: AppCompatActivity() {

    lateinit var uri : Uri

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tambah_barang)

        initView()
    }

    private fun initView() {
        supportActionBar?.title = "Tambah Barang Elektronik"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        button_tambah_foto_barang.setOnClickListener {
            val intent = Intent()
            intent.action = Intent.ACTION_GET_CONTENT
            intent.setType("image/*")
            startActivityForResult(intent, 100)
        }

        button_simpan_barang.setOnClickListener {
            uploadDataBarang()
        }
    }

    private fun uploadDataBarang() {
        val namaBarang = editText_Nama_Tambah_Barang.text.toString()
        val hargaBarang = editText_Harga_Tambah_Barang.text.toString()
        val deskripsiBarang = editText_deskripsi_tambah_barang.text.toString()
        val stokBarang = editText_Stok_Tambah_Barang.text.toString()

        Komponen.ProgressLoading(true,"Harap Tunggu...",this)
        val fileName = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
        val fileRef = FirebaseStorage.getInstance().reference.child(fileName+".jpg")
        fileRef.putFile(uri)
            .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                return@Continuation fileRef.downloadUrl
            })
            .addOnCompleteListener { task ->
                val idBarang = RandomAngka.getRandomString(15)
                FirebaseFirestore.getInstance().collection("products").document(idBarang).set(
                    Barang(idBarang,namaBarang,hargaBarang,deskripsiBarang,stokBarang.toInt(),fileName+".jpg",task.result.toString())
                )
                    .addOnSuccessListener {
                        Komponen.ProgressLoading(false,"",this)
                        onBackPressed()
                    }
                    .addOnFailureListener {
                        Toast.makeText(this, "Data failed to added ${it.message}", Toast.LENGTH_LONG).show()
                    }
            }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if((resultCode == Activity.RESULT_OK) && (requestCode == 100)){
            if(data != null){
                uri = data.data!!
                editText_selected_foto_tambah_barang.setText(uri.toString())
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}